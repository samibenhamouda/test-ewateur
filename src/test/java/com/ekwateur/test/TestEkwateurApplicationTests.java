package com.ekwateur.test;

import com.ekwateur.test.entities.ClientParticulier;
import com.ekwateur.test.entities.ClientPro;
import com.ekwateur.test.entities.Electricite;
import com.ekwateur.test.entities.Gaz;
import com.ekwateur.test.repositories.ClientParticulierRepository;
import com.ekwateur.test.repositories.ClientProRepository;
import com.ekwateur.test.repositories.ElectriciteRepository;
import com.ekwateur.test.repositories.GazRepository;
import com.ekwateur.test.services.Impl.FacturationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestEkwateurApplication.class)
class TestEkwateurApplicationTests {

    @Autowired
    private ClientProRepository clientProRepository;
    @Autowired
    private ClientParticulierRepository clientParticulierRepository;
    @Autowired
    private ElectriciteRepository electriciteRepository;
    @Autowired
    private GazRepository gazRepository;
    @Autowired
    private FacturationServiceImpl facturationService;

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void contextLoads() {
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_insertion_client_pro() {
        //Given
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(100l);

        //When
        clientProRepository.save(clientPro);
        Optional<ClientPro> client = clientProRepository.findByRefClient("EKW12345678");

        //Then
        Assertions.assertTrue(client.isPresent());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_insertion_client_particulier() {
        //Given
        ClientParticulier clientParticulier = new ClientParticulier();
        clientParticulier.setRefClient("EKW12345678");
        clientParticulier.setCivilite("civilite");
        clientParticulier.setNom("testNom");
        clientParticulier.setPrenom("testPrenom");

        //When
        clientParticulierRepository.save(clientParticulier);
        Optional<ClientParticulier> client = clientParticulierRepository.findByRefClient("EKW12345678");

        //Then
        Assertions.assertTrue(client.isPresent());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_insert_energie_electricite() {
        //Given
        Electricite electricite = new Electricite();

        //Then
        electriciteRepository.save(electricite);
        List<Electricite> electricites = electriciteRepository.findAll();

        //When
        Assertions.assertTrue(!electricites.isEmpty());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_insert_energie_gaz() {
        //Given
        Gaz gaz = new Gaz();

        //Then
        gazRepository.save(gaz);
        List<Gaz> gazs = gazRepository.findAll();

        //When
        Assertions.assertFalse(gazs.isEmpty());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_electricite_particuler() {
        //Given
        Electricite electricite = new Electricite();
        ClientParticulier clientParticulier = new ClientParticulier();
        clientParticulier.setRefClient("EKW12345678");
        clientParticulier.setCivilite("civilite");
        clientParticulier.setNom("testNom");
        clientParticulier.setPrenom("testPrenom");
        clientParticulier.setEnergies(Set.of(electricite));
        clientParticulierRepository.save(clientParticulier);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 100l);

        //Then
        Assertions.assertEquals(12.1, montant.doubleValue());

    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_electricite_pro_sup_ca() {
        //Given
        Electricite electricite = new Electricite();
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(20000000l);
        clientPro.setEnergies(Set.of(electricite));
        clientProRepository.save(clientPro);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 100l);

        //Then
        Assertions.assertEquals(11.4, montant.doubleValue());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_gaz_pro_sup_ca() {
        //Given
        Gaz gaz = new Gaz();
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(20000000l);
        clientPro.setEnergies(Set.of(gaz));
        clientProRepository.save(clientPro);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 45l);

        //Then
        Assertions.assertEquals(4.995, montant.doubleValue());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_electricite_gaz_pro_sup_ca() {
        //Given
        Electricite electricite = new Electricite();
        Gaz gaz = new Gaz();
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(20000000l);
        clientPro.setEnergies(Set.of(electricite, gaz));
        clientProRepository.save(clientPro);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 100l);

        //Then
        Assertions.assertEquals(22.5, montant.doubleValue());
    }


    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_gaz_pro_inf_ca() {
        //Given
        Gaz gaz = new Gaz();
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(9000l);
        clientPro.setEnergies(Set.of(gaz));
        clientProRepository.save(clientPro);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 80l);

        //Then
        Assertions.assertEquals(9.040000000000001, montant.doubleValue());
    }

    @Sql(scripts = "classpath:delete.sql")
    @Test
    void test_facture_gaz_pro_egale_ca() {
        //Given
        Gaz gaz = new Gaz();
        ClientPro clientPro = new ClientPro();
        clientPro.setRefClient("EKW12345678");
        clientPro.setNumSiret("123456");
        clientPro.setRaisonSociale("test");
        clientPro.setChiffreAffaire(1000000l);
        clientPro.setEnergies(Set.of(gaz));
        clientProRepository.save(clientPro);

        //When
        Double montant = facturationService.calculMontant("EKW12345678", 100l);

        //Then
        Assertions.assertEquals(11.3, montant.doubleValue());
    }

    @Test
    void verifier_ref_client_true() {
        //Given
       String refClient = "EKW12345678";

        //When
       boolean verif = facturationService.verifRefClient(refClient);

        //Then
        Assertions.assertTrue(verif);
    }

    @Test
    void verifier_ref_client_false() {
        //Given
        String refClient = "EW12345678";

        //When
        boolean verif = facturationService.verifRefClient(refClient);

        //Then
        Assertions.assertFalse(verif);
    }

}
