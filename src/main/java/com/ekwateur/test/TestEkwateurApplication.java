package com.ekwateur.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestEkwateurApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestEkwateurApplication.class, args);
    }

}
