package com.ekwateur.test.utils;

public record RequestFacturation(String refClient, Long nbKwh) {
}
