package com.ekwateur.test.entities;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue("pro")
public class ClientPro extends Client {

    private String numSiret;
    private String raisonSociale;
    private Long chiffreAffaire;

    public String getNumSiret() {
        return numSiret;
    }

    public void setNumSiret(String numSiret) {
        this.numSiret = numSiret;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public Long getChiffreAffaire() {
        return chiffreAffaire;
    }

    public void setChiffreAffaire(Long chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }
}
