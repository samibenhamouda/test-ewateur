package com.ekwateur.test.contollers;

import com.ekwateur.test.services.FacturationService;
import com.ekwateur.test.utils.RequestFacturation;
import com.ekwateur.test.utils.ResultatFacturation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/v1")
public class FacturationController {

    private final static Logger logger = LoggerFactory.getLogger(FacturationController.class);

    @Autowired
    private FacturationService facturationService;


    @PostMapping(path = "/facturation")
    public ResponseEntity<?> calculMontant(@RequestBody RequestFacturation requestFacturation) {
        try {
            if (facturationService.verifRefClient(requestFacturation.refClient())) {
                Double montant = facturationService.calculMontant(requestFacturation.refClient(), requestFacturation.nbKwh());
                return new ResponseEntity<>(new ResultatFacturation(montant), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("La référence du client est incorrecte", HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            logger.error("erreur au niveau du ws facturation, refClient = [{}]", requestFacturation.refClient(), e);
            throw e;
        }
    }
}
