package com.ekwateur.test.repositories;

import com.ekwateur.test.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ClientRepository<T extends Client> extends JpaRepository<T, Long> {

    Optional<T> findByRefClient(String refClient);
}
