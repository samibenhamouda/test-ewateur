package com.ekwateur.test.repositories;

import com.ekwateur.test.entities.ClientParticulier;

public interface ClientParticulierRepository extends ClientRepository<ClientParticulier> {
}
