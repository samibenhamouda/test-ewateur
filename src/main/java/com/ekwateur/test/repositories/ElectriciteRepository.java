package com.ekwateur.test.repositories;

import com.ekwateur.test.entities.Electricite;

public interface ElectriciteRepository extends EnergieRepository<Electricite> {

}
