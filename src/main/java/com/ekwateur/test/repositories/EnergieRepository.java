package com.ekwateur.test.repositories;

import com.ekwateur.test.entities.Energie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnergieRepository<T extends Energie> extends JpaRepository<T, Long> {

}
