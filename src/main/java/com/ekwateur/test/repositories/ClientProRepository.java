package com.ekwateur.test.repositories;

import com.ekwateur.test.entities.ClientPro;

public interface ClientProRepository extends ClientRepository<ClientPro> {
}
