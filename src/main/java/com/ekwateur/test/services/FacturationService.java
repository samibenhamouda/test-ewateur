package com.ekwateur.test.services;

public interface FacturationService {

    Double calculMontant(String refClient, Long nbKw);

    boolean verifRefClient(String refClient);
}
