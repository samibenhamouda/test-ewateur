package com.ekwateur.test.services.Impl;

import com.ekwateur.test.entities.ClientParticulier;
import com.ekwateur.test.entities.ClientPro;
import com.ekwateur.test.entities.Electricite;
import com.ekwateur.test.entities.Energie;
import com.ekwateur.test.repositories.ClientParticulierRepository;
import com.ekwateur.test.repositories.ClientProRepository;
import com.ekwateur.test.services.FacturationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class FacturationServiceImpl implements FacturationService {


    @Autowired
    private ClientParticulierRepository clientParticulierRepository;
    @Autowired
    private ClientProRepository clientProRepository;

    @Value("${regex.ref.client}")
    private String regexRefClient;

    public static Double PRIX_PART_ELECTIRICITE = 0.121;
    public static Double PRIX_PART_GAZ = 0.115;
    public static Double PRIX_PRO_ELECTIRICITE_SUP_CA = 0.114;
    public static Double PRIX_PRO_GAZ_SUP_CA = 0.111;
    public static Double PRIX_PRO_ELECTIRICITE_INF_CA = 0.118;
    public static Double PRIX_PRO_GAZ_INF_CA = 0.113;
    public static final int CHIFFRE_AFFAIRE = 1_000_000;

    public Double calculMontant(String refClient, Long nbKw) {
        Double result = 0.0;
        Optional<ClientParticulier> clientParticulier = clientParticulierRepository.findByRefClient(refClient);
        if (clientParticulier.isPresent()) {
            result = calculerPrixEnergie(clientParticulier.get().getEnergies(), nbKw, PRIX_PART_ELECTIRICITE, PRIX_PART_GAZ);
        } else {
            Optional<ClientPro> clientPro = clientProRepository.findByRefClient(refClient);
            if (clientPro.isPresent()) {
                if (clientPro.get().getChiffreAffaire() > CHIFFRE_AFFAIRE) {
                    result = calculerPrixEnergie(clientPro.get().getEnergies(), nbKw, PRIX_PRO_ELECTIRICITE_SUP_CA, PRIX_PRO_GAZ_SUP_CA);
                } else {
                    result = calculerPrixEnergie(clientPro.get().getEnergies(), nbKw, PRIX_PRO_ELECTIRICITE_INF_CA, PRIX_PRO_GAZ_INF_CA);
                }
            }
        }
        return result;
    }

    @Override
    public boolean verifRefClient(String refClient) {
        return refClient.matches(regexRefClient);
    }

    private Double calculerPrixEnergie(Set<Energie> energies, Long nbKw, double prixElectricite, double prixGaz) {
        double somme = 0;
        for (Energie energie : energies) {
            if (energie instanceof Electricite) {
                somme += prixElectricite * nbKw;
            } else somme += prixGaz * nbKw;
        }
        return somme;
    }
}
