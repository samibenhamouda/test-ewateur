/*client pro*/
insert into clients (id,ref_client, num_siret, raison_sociale, chiffre_affaire, client_type)
values (1,'EKW14785473', '123456', 'test', 2000000, 'pro');
insert into clients (id,ref_client, num_siret, raison_sociale, chiffre_affaire, client_type)
values (2,'EKW58699874', '123456', 'test', 101, 'pro');
insert into clients (id,ref_client, num_siret, raison_sociale, chiffre_affaire, client_type)
values (3,'EKW45218005', '123456', 'test', 102, 'pro');

/*client particulier*/
insert into clients (id,ref_client, civilite, nom, prenom, client_type)
values (4,'EKW74447502', 'civilite', 'test', 'test', 'particulier');
insert into clients (id,ref_client, civilite, nom, prenom, client_type)
values (5,'EKW12345678', 'civilite', 'test', 'test', 'particulier');
insert into clients (id,ref_client, civilite, nom, prenom, client_type)
values (6,'EKW66655547', 'civilite', 'test', 'test', 'particulier');

/*energie*/
insert into energie (id, energie_type)
values (1, 'electricite');
insert into energie (id, energie_type)
values (2, 'gaz');

/*client_energie*/
insert into client_energie (id_client, id_energie)
values (1, 1);
insert into client_energie (id_client, id_energie)
values (1, 2);
insert into client_energie (id_client, id_energie)
values (2, 2);
insert into client_energie (id_client, id_energie)
values (4, 1);


