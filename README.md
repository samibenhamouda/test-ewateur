# Spécification fonctionnelle

Calcule du montant à facturer à un client d'Ekwateur pour un mois calendaire.

## Description du WS

- Le projet expose un web service POST pour le calcul du mantant.
- Le web service utilise une base de donnée en mémoire H2.
- La scructure de la requête:
  {
  "refClient": "string",
  "nbKwh": 0 }
- La structure du resultat:
  {
  "montant": 0.0 }
- Url-swagger: http://localhost:8081/swagger-ui/index.html
- Login : admin , password : admin

## Pour tester le WS

Le tableau suivant présente des JDD pour le test:

- Table clients:

| id  | client_type|ref_client |num_siret| raison_sociale| chiffre_affaire| civilite| nom| prenom |
| --- | -----------|---------| --------------| ---------------| --------|----|--------|-----------|
| 1   |pro         | EKW14785473 |123456   |test           |2000000         |
| 2 | pro |EKW58699874| 123456 | test|101|

- Table Energie:

| id  | energie_type|
| --- | -----------|
| 1   |electricite         | 
| 2 | gaz |EKW58699874|

- Table client_energie:

| id_client  | id-energie|
| --- | -----------|
| 1   |1         | 
| 1 | 2 |
